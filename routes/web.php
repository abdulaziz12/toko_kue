<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('/kue/','KueController@create');
$router->get('/kue/','KueController@read');
$router->post('/kue/{id}','KueController@update');
$router->delete('/kue/{id}','KueController@delete');
$router->get('/kue/{id}','KueController@detail');

//bahan
$router->post('/bahan/','BahanController@create');
$router->get('/bahan/','BahanController@read');
$router->post('/bahan/{id}','BahanController@update');
$router->delete('/bahan/{id}','BahanController@delete');
