<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kue extends model
{
  public $table = 't_kue';

  protected $fillable =[
    'id','nama_kue','harga','keterangan'
  ];
}
?>
