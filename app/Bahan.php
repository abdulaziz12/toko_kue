<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bahan extends model
{
  public $table = 't_bahan';

  protected $primaryKey ='id_bahan'
  protected $fillable =[
    'id','nama_bahan','harga','keterangan'
  ];
}
?>
