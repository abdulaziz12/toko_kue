import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Kue from '@/components/Kue'
import KueForm from '@/components/KueForm'
import Bahan from '@/components/Bahan'
import BahanForm from '@/components/BahanForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/kue',
      name: 'Kue',
      component: Kue
    },
    {
      path: '/kue/create',
      name: 'KueCreate',
      component: KueForm
    },
    {
      path: '/kue/:id',
      name: 'KueEdit',
      component: KueForm
    },
    {
      path: '/bahan',
      name: 'Bahan',
      component: Bahan
    },
    {
      path: '/bahan/create',
      name: 'BahanCreate',
      component: BahanForm
    },
    {
      path: '/bahan/:id',
      name: 'BahanEdit',
      component: BahanForm
    }
  ]
})
